const express = require('express');
// const morgan = require('morgan');
var bodyParser = require('body-parser');
const dbConnector = require('./dataBase/db_connect.js');
const loginModule = require('./src/login.js');

const routes = require('./routes/routes.js')

const app = express();

app.set('views', __dirname + '/views')//Determina en donde estan las vistas para el render
app.set('view engine', 'ejs');

loginModule.login(app, dbConnector);

const hostname = '127.0.0.1';
const port = 3000;

//rutas
app.use(routes);

app.listen(port, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
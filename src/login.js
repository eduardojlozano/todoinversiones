var session = require('express-session');
var bodyParser = require('body-parser');
var path = require('path');

exports.login = (app, db) => {
	app.use(session({
		secret: 'secret',
		resave: true,
		saveUninitialized: true
	}));
	app.use(bodyParser.urlencoded({extended : true}));
	app.use(bodyParser.json());  

	app.post('/auth', function(request, response) {
		var username = request.body.username;
		var password = request.body.password;
		if (username && password) {
			db.connectObj.query('SELECT * FROM cliente WHERE user = ? AND pass = ?', [username, password], function(error, results, fields) {
				if(error){
					console.log('Error: ', error);
				}
	
				if (results.length > 0) {
					request.session.loggedin = true;
					request.session.username = username;
					response.redirect('/home');
				} else {
					response.send('Incorrect Username and/or Password!');
				}			
				response.end();
			});
		} else {
			response.send('Please enter Username and Password!');
			response.end();
		}
	});
}


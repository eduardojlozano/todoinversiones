const express = require('express');
const router = express.Router();


router.get('/', (req, res) => {
	res.render('index.ejs')
})

router.get('/login', (req, res) => {
	res.render('login.ejs');
});

router.get('/home', (req, resp) => {
	if(req.session.loggedin){
		resp.render('home.ejs');
		resp.end()
	}else{
		resp.end('Please login to view this page!');	
	}
});

module.exports = router;
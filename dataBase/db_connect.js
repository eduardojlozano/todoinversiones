var mysql = require('mysql');

var connection = mysql.createConnection({
	host     : 'localhost',
	user     : 'root',
	password : '12345',
	database : 'tpes2019'
});

connection.connect(function(err) {
	if (err) throw err;
	console.log("Connected to MySql database!");
});

exports.connectObj = connection;

